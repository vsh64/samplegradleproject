package Annotations.Week5.day1;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {
	static ExtentHtmlReporter html;
	static ExtentReports extent;
	public ExtentTest test;
	public int i=1;
	String testcaseName,testDesc,author,category;
	
	@BeforeSuite
	public void startResult() {
		html=new ExtentHtmlReporter("./reports/result["+i+"].html");i++;
		html.setAppendExisting(true);
    	extent =new ExtentReports();
		extent.attachReporter(html);
		
	}
		
	@BeforeMethod
	public void startTestCase() {
		test = extent.createTest(testcaseName, testDesc);
		test.assignAuthor(author);
		test.assignCategory(category);

	}
		
	
	
	
	public void LogStep(String status,String Desc) throws IOException{
		if(status.equalsIgnoreCase("PASS"))
			test.pass(Desc);
		//test.pass("userName entered",MediaEntityBuilder.createScreenCaptureFromPath("./../snapshots/img1["+i+"].png").build());
		else if(status.equalsIgnoreCase("FAIL"))
		//test.fail("UserName Not enetered", MediaEntityBuilder.createScreenCaptureFromPath("./../snapshots/img1["+i+"].png").build());
		test.fail(Desc);
			else if(status.equalsIgnoreCase("WARN"))
			test.warning(Desc);
	
		
		
	}
	
	@AfterSuite
	public void endResult() {
		extent.flush();
		
	}

}
