package Annotations.Week5.day1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class AnnotationHierarchyTest {
  @Test  public void Test1() {System.out.println("Test1");}
  @Test  public void Test2() {System.out.println("Test1");}
  @Test  public void Test3() {System.out.println("Test1");}
  @BeforeMethod public void beforeMethod() {System.out.println("beforeMethod");}
  @BeforeMethod public void beforeMethod1() {System.out.println("beforeMethod1");}
  @AfterMethod  public void afterMethod() {System.out.println("afterMethod");}
  @AfterMethod  public void afterMethod1() {System.out.println("afterMethod1");}
  @BeforeClass  public void beforeClass() {System.out.println("beforeClass");}
  @AfterClass  public void afterClass() {System.out.println("afterClass");}
  @BeforeClass  public void beforeClass1() {System.out.println("beforeClass1");}
  @AfterClass  public void afterClass1() {System.out.println("afterClass1");}
  @BeforeTest  public void beforeTest() {System.out.println("beforeTest");}
  @AfterTest  public void afterTest() {System.out.println("afterTest");}
  @BeforeTest  public void beforeTest1() {System.out.println("beforeTest1");}
  @AfterTest  public void afterTest1() {System.out.println("afterTest1");}
  @BeforeSuite  public void beforeSuite() {System.out.println("beforeSuite");}
  @AfterSuite  public void afterSuite() {System.out.println("afterSuite");}
  @BeforeSuite  public void beforeSuite1() {System.out.println("beforeSuite1");}
  @AfterSuite  public void afterSuite1() {System.out.println("afterSuite1");}

}
