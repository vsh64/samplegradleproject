package Annotations.Week5.day1;

import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TestCase3 extends ProjectSpecificMethod {
	
	@Test
	public void Login() throws Throwable {
		
		
		WebElement d = locate("LinkText", "CRM/SFA");
		click(d);
		WebElement e = locate("LinkText", "Create Lead");
		click(e);
		WebElement f = locate("id", "createLeadForm_companyName");
		enter(f, "IBM");
		WebElement g = locate("id", "createLeadForm_firstName");
		enter(g, "Vishnu");
		WebElement h = locate("id", "createLeadForm_lastName");
		enter(h, "Vsh");
		WebElement i = locate("id", "createLeadForm_personalTitle");
		enter(i, "Mr.");
		WebElement j = locate("id", "createLeadForm_firstNameLocal");
		enter(j, "Vshu");
		WebElement k = locate("id", "createLeadForm_lastNameLocal");
		enter(k, "Vshuuuu");
		WebElement l = locate("id", "createLeadForm_dataSourceId");
		selectDropDownByVisibleText(l, "Direct Mail");
		WebElement m = locate("id", "createLeadForm_industryEnumId");
		selectDropDownByVisibleText(m, "Finance");
		WebElement n = locate("class", "smallSubmit");
		click(n);
		
		
	}
}
