package Annotations.Week5.day1;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;

public class ProjectSpecificMethod extends MyMethods{
	@BeforeMethod
	public void doLogin() throws Throwable{

	startApp("chrome", "http://leaftaps.com/opentaps/");
	WebElement a = locate("name","USERNAME");
	enter(a, "DemoSalesManager");
	WebElement b = locate("name", "PASSWORD");
	enter(b, "crmsfa");
	WebElement c = locate("class", "decorativeSubmit");
	click(c); 
	

	}

}
