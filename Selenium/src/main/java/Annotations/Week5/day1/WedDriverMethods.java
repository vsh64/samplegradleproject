package Annotations.Week5.day1;

import java.io.IOException;

import org.openqa.selenium.WebElement;

public interface WedDriverMethods {
	
	public void startApp(String browser,String url);
	public String enter(WebElement ele , String data) throws IOException;
	public void click(WebElement ele ) throws IOException;
	public WebElement locate(String attribute,String value);
	public void screenshot();
	public WebElement locate(String value);
	WebElement selectDropDownByVisibleText(WebElement ele, String value);
	WebElement selectDropDownByIndex(WebElement ele, int value);
	WebElement selectDropDownByValue(WebElement ele, String value);
	public void close();
	 

}
