package DailyQuestions;

import java.util.Scanner;

public class SecondSmallestNumberConditionalMethod {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter inputs");
		
		int a= sc.nextInt();
		int b= sc.nextInt();
		int c= sc.nextInt();
		
		System.out.println("First Number:"+a);
		System.out.println("Second Number:"+b);
		System.out.println("Third Number:"+c);
		
		boolean  x=(a<b && b<c) || (c<b && b<a);
		boolean  y=(b<a && a<c) || (c<a && a<b);
		boolean  z=(b<c && c<a) || (a<c && c<b) ;
		
		if(x)
			System.out.println(b+"is the second smallest Number");
		else if(y)
			System.out.println(a+" ***is the second smallest Number");
		else if(z)
			System.out.println(c+" *is the second smallest Number");
		else
			System.out.println("All are Equal");
		}

}
