package DailyQuestions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws Throwable {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		
		Set<String> allWindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allWindows);
		driver.switchTo().window(lst.get(1));
		driver.findElementByName("id").sendKeys("10112");
	    driver.findElementByXPath("//button[text()='Find Leads']").click();

	    Thread.sleep(10000);
	    driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(lst.get(0));
		 //  driver.switchTo().defaultContent();

	    Thread.sleep(10000);
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		Set<String> allWindows1 = driver.getWindowHandles();
		List<String> lst2 = new ArrayList<>();
		lst2.addAll(allWindows1);
		driver.switchTo().window(lst2.get(1));
	    Thread.sleep(10000);
System.out.println("****");
		driver.findElementByName("id").sendKeys("10113");
	    driver.findElementByXPath("//button[text()='Find Leads']").click();
	    Thread.sleep(10000);

	    driver.findElementByXPath("(//a[@class='linktext'])[3]").click();
		driver.switchTo().window(lst2.get(0));

	    
	    driver.findElementByLinkText("Merge").click();
	    
	    Alert alert=driver.switchTo().alert();
	    alert.accept();

	    driver.findElementByLinkText("Find Leads").click();
	    driver.findElementByName("id").sendKeys("10112");
	    driver.findElementByXPath("//button[text()='Find Leads']").click();
	    Thread.sleep(10000);
	    WebElement error = driver.findElementByXPath("//div[text()='No records to display']");
	    String text=error.getText();
	    System.out.println(text);
	    
	    
	 //   driver.findElementByXPath("(//a[@class='linktext'])[1]").click();



	
		System.out.println("*****************");

		
		
		
		

	}

}
