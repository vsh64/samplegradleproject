package DailyQuestions;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {

		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the number:");
		int num= sc.nextInt();
		int b,sum=0,a=num;

		while(num>0) {
			b=num%10;
			sum=sum*10+b;
			num=num/10;
		}
	
		if(sum==a)	System.out.println("Palindrome");
		else System.out.println("Not a Palindrome");
	}

}
