package DailyQuestions;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Number ");
		int n = sc.nextInt();
		int a = 0, b = 1, temp = 1, i = 1;

		for (i = 1; i <= n; i++) {

			System.out.print(a + " ");
			a = a + b;
			b = a - b;

		}

		/*
		 * while(i<=n) { System.out.print(a+" "); a=b; b=temp; temp=a+b;
		 * 
		 * i++; }
		 */
	}
}
