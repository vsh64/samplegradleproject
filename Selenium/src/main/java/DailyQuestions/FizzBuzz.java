package DailyQuestions;

import java.util.Scanner;

public class FizzBuzz {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter numbers");
		int n1=sc.nextInt();
		int n2=sc.nextInt();
		
		while (n1<=n2) {
		//	System.out.println(n1);
			
			if((n1%3==0) && (n1%5==0)) System.out.print(" FIZZBUZZ ");
			else if(n1%3==0) System.out.print(" FIZZ ");
			else if(n1%5==0) System.out.print(" BUZZ ");
			else if((n1%3!=0) && (n1%5!=0)) System.out.print(n1);
			else System.out.print(" Do Nothing ");
			
			n1++;
			
		}
	}

}
