package DailyQuestions;

public class MultiLevelInheritanceSample {

	public static void main(String[] args) {
		
		Audi audi = new Audi(); 
		
		/* The method is called automatically whenever you instantiate 
		 * (create) an instance of your class.
		 Typically, a constructor is used to initialize 
		 class fields regardless of their access 
		 modifiers (private/protected/public).
		 If no constructor is defined, it means
		  the language provides no arg constructor that 
		  does nothing.*/
		
	}

}
