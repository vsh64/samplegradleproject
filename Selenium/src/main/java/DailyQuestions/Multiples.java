package DailyQuestions;

import java.util.Scanner;

public class Multiples {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number:");
		int a=sc.nextInt();
		int i=1,sum=0;
		for ( i = 1; i <=a; i++) {
			if(i%3==0 || i%5==0) 
				sum=sum+i;
		}	
		System.out.println(sum);
	}

	
}
