package DailyQuestions;

import java.util.Scanner;

public class SecondSmallestNumberSortingMethod{

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Inputs");
		int size = sc.nextInt();
		int[] Num = new int[size];
		int temp = 0,i=0,j=0;

		for ( j = 0; j < size; j++)
			Num[j] = sc.nextInt();

		/**
		  *SMALLEST NUMBER 
		  **/

		for ( j = 0; j < size; j++)
		{
			for (i = j + 1; i < size; i++) 
			{
				if (Num[j] > Num[i])
				{
					temp = Num[j];
					Num[j] = Num[i];
					Num[i] = temp;
				}
			}
		}
		System.out.print(Num[1] + " is the second smallest Number");

	}
}

	
