package DailyQuestions;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordRegex {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Password:");
		String str=sc.next();
		String pat= "[[[a-z][A-Z]{1,}]{2,}[\\d]{2,}]{10,}";
		
		Pattern p=Pattern.compile(pat);
		Matcher m=p.matcher(str);
		boolean matches = m.matches();
		if(matches)
		System.out.println("Valid Password");
		else System.out.println("Invalid Password");
		
	}

}
