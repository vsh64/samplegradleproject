package DailyQuestions;

import 
java.util.Scanner;

public class SumOfGivenNumbers {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of Numbers:");
		int size = sc.nextInt();
		int sum = 0, i = 0;

		int[] num = new int[size];
		System.out.println("Enter the Numbers:");
		for (i = 0; i < size; i++) {
			num[i] = sc.nextInt();
		}
		for (i = 0; i < size; i++) {
			sum += num[i];
		}
		System.out.println("Sum of numbers :" + sum);

	}

}
