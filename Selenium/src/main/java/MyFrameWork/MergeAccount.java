package MyFrameWork;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MergeAccount extends CommonMethods{
	
	@BeforeClass
	public void setUp() {
		
		TestCaseName = "TC_01 MergeAccount";
		TestCaseDescription="Merging two Accounts";
		Author ="Vishnu";
		TestingType="Regression";
			}
	
	
	@Test
	public void MergeAccount() throws IOException {
		
		start("Chrome", "http://leaftaps.com/opentaps/");
		WebElement username = Locate("id", "username");
		InputText(username, "DemoSalesManager");
		WebElement password = Locate("id", "password");
		InputText(password, "crmsfa");
		WebElement submit = Locate("class", "decorativeSubmit");
		submit.click();
		WebElement Link = Locate("LinkText", "CRM/SFA");
		Link.click();
		WebElement Account = Locate("LinkText", "Accounts");
		click(Account);
	    WebElement MergeAccount = Locate("LinkText", "Merge Accounts");
	    click(MergeAccount);
	    WebElement icon1=Locate("xpath", "(//img[@alt='Lookup'])[1]");
	    click(icon1);
	    switchToWindow(1);
	    WebElement AccountID = Locate("xpath", "//input[@name='id']");
	    InputText(AccountID, "10583");
	    WebElement FindAccount=Locate("xpath", "//button[text()='Find Accounts']");
	    click(FindAccount);
	    WebElement FirstID=Locate("class", "linktext");
	    click(FirstID);
	    switchToWindow(0);
	    WebElement icon2=Locate("xpath", "(//img[@alt='Lookup'])[2]");
	    click(icon2);
	    switchToWindow(1);
	    WebElement AccountID2 = Locate("xpath", "//input[@name='id']");
	    InputText(AccountID2, "10586");
	    WebElement FindAccount1=Locate("xpath", "//button[text()='Find Accounts']");
	    click(FindAccount1);
	    WebElement FirstID2=Locate("class", "linktext");
	    click(FirstID2);
	    switchToWindow(0);
	    WebElement Merge= Locate("LinkText", "Merge");
	    click(Merge);
	    alert("accept", "");
	    WebElement FindAccounts= Locate("LinkText", "Find Accounts");
	    click(FindAccounts);
	    WebElement FindAccountID = Locate("xpath", "//input[@name='id']");
	    InputText(FindAccountID, "10583");
	    WebElement FindAccount3=Locate("xpath", "//button[text()='Find Accounts']");
	    click(FindAccount3);
	    WebElement ele = Locate("xpath", "//div[text()='No records to display']");
	    VerifyExactText(ele, "No records to display");
	    quit();
	    
	    
	    
	    
	    
	    
	    
	    
	    

		
		
	}
	
	

}
