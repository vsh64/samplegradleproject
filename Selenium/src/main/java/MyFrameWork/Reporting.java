package MyFrameWork;

import java.io.IOException;
import java.time.LocalTime;
import java.time.temporal.Temporal;

import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reporting {
	
	static ExtentHtmlReporter html;
	static ExtentReports extent;
    public	ExtentTest test;
    String TestCaseName ,TestCaseDescription,Author,TestingType;
	
    @BeforeSuite
	public void startCase() {
	
		 //LocalTime now = java.time.LocalTime.now();
		 html= new ExtentHtmlReporter("./HTML Reports/report.html");
		html.setAppendExisting(true);
	    extent= new ExtentReports();
		extent.attachReporter(html);
		
	}
    @BeforeMethod
    public void testStart() {
		test = extent.createTest(TestCaseName,TestCaseDescription);
		test.assignAuthor(Author);
		test.assignCategory(TestingType);


    }
    public void Report(String status,String Description) throws IOException {
		
		try {
			if (status.equalsIgnoreCase("PASS"))
				test.pass(Description);
		} catch (Exception e) {

			if (status.equalsIgnoreCase("Fail"))
				test.fail(Description);
			e.printStackTrace();
		}
		
	}

    @AfterSuite
    public void flush() {
		extent.flush();

    }

}
