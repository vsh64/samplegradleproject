package MyFrameWork;

import java.io.IOException;

import org.openqa.selenium.WebElement;

public interface MyInterface {
	
	public void start(String browser, String url) throws IOException;
	public void takeScreenshot() throws IOException;
	public void close();
	public void quit();
	public void InputText(WebElement Locator, String text);
	public void click(WebElement Locate);
	public WebElement Locate(String locator , String locator_value);
	public WebElement Locate(String locator);
	public String getText(String locator, String locator_value);
	public boolean VerifyTitle(String expectedTitle);
	public boolean VerifyExactText(WebElement ele,String expectedText );
	public boolean VerifyPartialText(String PartialText ,String locator ,String locator_value);
	public boolean VerifySelected(String locator,String  locator_value);
	public boolean VerifyDisplayed(String locator,String  locator_value);
	public void switchToWindow(int index);
	public void switchToFrame(WebElement ele);
	public WebElement alert(String action ,String text);
	public boolean  verifyExactAttribute(String locator , String Att_name, String expectedAttribute);
	public boolean  verifyPartialAttribute(String locator , String Att_name, String expectedAttribute) ;
	public WebElement selectDropDownByTextorValue(WebElement ele, String value);
	public WebElement selectDropDownByIndex(WebElement ele, int value);

}
