package MyFrameWork;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CreateLead extends CommonMethods{
	


	@BeforeClass
	public void setUp() {
		TestCaseName= "TC_CreateLead";
		TestCaseDescription="Creating a Lead";
		Author="vishnu";
		TestingType="Regression";
		
	}
	
    
	@Test
	public void TC01() throws IOException {
		start("chrome", "http://leaftaps.com/opentaps/");
		WebElement username = Locate("id", "username");
		InputText(username, "DemoSalesManager");
		WebElement password = Locate("id", "password");
		InputText(password, "crmsfa");
		WebElement submit = Locate("class", "decorativeSubmit");
		submit.click();
		WebElement Link = Locate("LinkText", "CRM/SFA");
		Link.click();
		WebElement CreateLead = Locate("LinkText", "Create Lead");
		CreateLead.click();
		WebElement ComName = Locate("id", "createLeadForm_companyName");
		InputText(ComName, "IBM");
		WebElement firstName = Locate("id", "createLeadForm_firstName");
		InputText(firstName, "Vishnu");
		WebElement lastName = Locate("id", "createLeadForm_lastName");
		InputText(lastName, "Vsh");
		//WebElement submitButton = Locate("xpath", "//input[@name='submitButton']");
	//	submitButton.click();
		WebElement select = Locate("id", "createLeadForm_industryEnumId");
		selectDropDownByTextorValue(select, "Aerospace");

		
		
		
		
		
		
		
		
		
		
		
	}

}
