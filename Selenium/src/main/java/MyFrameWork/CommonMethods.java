package MyFrameWork;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class CommonMethods extends Reporting implements MyInterface {

	RemoteWebDriver driver;
	public int i = 1;
	private String text;

	@Override // **********Launch Browser**************//

	public void start(String browser, String url) throws IOException {

		try {
			if (browser.equalsIgnoreCase("Chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("IE")) {
				System.setProperty("webdriver.ie.driver", "./drivers/iedriver.exe");
				driver = new InternetExplorerDriver();
			} else if (browser.equalsIgnoreCase("FireFox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			System.out.println(browser + " Browser Launched Successfully");
		} 
		catch (Exception e) {
			System.out.println(browser + " Not Launched ");

			e.printStackTrace();
		}

		try {
			driver.get(url);
			System.out.println(url + " opened successfully");
		} catch (Exception e) {
			System.out.println("Incorrect url or check with internet Connection");
			e.printStackTrace();
		}

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		takeScreenshot();
	}

	/****************************************************************************/
	@Override
	public void takeScreenshot() throws IOException {

		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File des = new File("./screenshots/snap[" + i + "].png");
			FileUtils.copyFile(src, des);
			i++;
		} catch (WebDriverException e) {
			System.out.println("screenshot cannot be taken");
			e.printStackTrace();
		}
	}

	/****************************************************************************/

	@Override
	public void close() {
		try {
			driver.close();
			System.out.println("closed");
		} catch (Exception e) {
			System.out.println("Driver didnot close");
			e.printStackTrace();
		}
	}

	/****************************************************************************/

	@Override
	public void InputText(WebElement Locator, String text) {

		try {
			Locator.clear();
			Locator.sendKeys(text);
			takeScreenshot();
			System.out.println(text + " Entered");
		} catch (Exception e) {
			System.out.println("Input text field Not Found");
			e.printStackTrace();
		}
	}

	/****************************************************************************/
	public WebElement Locate(String locator, String locator_value) {

		switch (locator) {

		case "class":
			return driver.findElementByClassName(locator_value);
		case "id":
			return driver.findElementById(locator_value);
		case "xpath":
			return driver.findElementByXPath(locator_value);
		case "LinkText":
			return driver.findElementByLinkText(locator_value);
		case "PartialLinkText":
			return driver.findElementByPartialLinkText(locator_value);
		case "Name":
			return driver.findElementByName(locator_value);
		case "TagName":
			return driver.findElementByTagName(locator_value);
		}
		return null;
	}

	/****************************************************************************/

	@Override
	public void quit() {

		driver.quit();
		System.out.println("All opened Browsers closed");
	}

	/****************************************************************************/

	@Override
	public void click(WebElement Locate) {
		try {
			Locate.click();
			System.out.println(Locate.getText()+" Clicked");
			takeScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/****************************************************************************/

	@Override
	public WebElement Locate(String locator) {
		WebElement UniLocator = driver.findElementById(locator);
		return null;
	}

	/****************************************************************************/

	public String getText(String locator, String locator_value) {

		switch (locator) {
		case "class":
			return driver.findElementByClassName(locator_value).getText();
		case "id":
			return driver.findElementById(locator_value).getText();
		case "xpath":
			return driver.findElementByXPath(locator_value).getText();
		case "LinkText":
			return driver.findElementByLinkText(locator_value).getText();
		case "PartialLinkText":
			return driver.findElementByPartialLinkText(locator_value).getText();
		case "Name":
			return driver.findElementByName(locator_value).getText();
		case "TagName":
			return driver.findElementByTagName(locator_value).getText();

		}
		return null;
	}

	/****************************************************************************/

	@Override
	public boolean VerifyTitle(String expectedTitle) {

		if (driver.getTitle().contains(expectedTitle)) {
			System.out.println("title Verified");
			return true;
		} else {
			System.out.println("Title Verification Failed");
			return false;
		}
	}

	/****************************************************************************/

	public boolean VerifyExactText(WebElement Locate,String expectedText ) {
		String text = Locate.getText();
		//String text = getText(locator, locator_value);
		if(text.equals(expectedText)) {
			System.out.println("Text Matched");
		return true;
		}
		else {
			System.out.println("Text Does not Match"); 
			return false;
		}
		}
	/****************************************************************************/

	@Override
	public boolean VerifyPartialText(String PartialText,String locator ,String locator_value) {
		String text = getText(locator, locator_value);
		if(text.contains(PartialText)) {
			System.out.println("Partial Text Verified");
		return true;
		}
		else  { System.out.println("Partial text Not Verified");
		return false;
		}
	}
	/****************************************************************************/

	public boolean VerifySelected(String locator,String  locator_value) {
		WebElement ele = Locate(locator, locator_value);
		boolean selected = ele.isSelected();
		if(selected) {
			System.out.println("Selected");
			return true;
		}
		else 
		{
			System.out.println("Not selected");
		return false;
	}
	}

	/****************************************************************************/

	@Override
	public boolean VerifyDisplayed(String locator, String locator_value) {
		WebElement ele = Locate(locator, locator_value);
		boolean displayed = ele.isDisplayed();
		if(displayed) {
			System.out.println("Displayed");
			return true;
		}
		else 
		{
			System.out.println("Not Displayed");
		return false;
	}
	}
	/****************************************************************************/

	@Override
	public void switchToWindow(int index) {

		Set<String> allwindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<String>();
		lst.addAll(allwindows);
		driver.switchTo().window(lst.get(index));
	}

	/****************************************************************************/

	@Override
	public void switchToFrame(WebElement nameOrId) {
		
		driver.switchTo().frame(nameOrId);
		
	}

	public WebElement  alert(String action,String text) {
		try {
			Alert a=driver.switchTo().alert();
			switch (action) {
			case "accept":  a.accept();
			case "dismiss":  a.dismiss();
			case "getAlertText": a.getText();
			case "sendKeys": a.sendKeys(text);
			}
		
		} catch (NoAlertPresentException e) {
			System.out.println("Alert not Present");
			e.printStackTrace();
		} catch (UnhandledAlertException e) {
			System.out.println("Unhandled Exception");
			e.printStackTrace();

			
		}
		return null;
	}

		
		/****************************************************************************/
	

	@Override
	public boolean  verifyExactAttribute(String locator , String Att_name, String expectedAttribute) {

		if(driver.findElementById(locator).getAttribute(Att_name).equals(expectedAttribute))
				return true;
		else return false ;
	}
	
	/****************************************************************************/
	@Override
	public boolean  verifyPartialAttribute(String locator , String Att_name, String expectedAttribute) {

		if(driver.findElementById(locator).getAttribute(Att_name).contains(expectedAttribute))
				return true;
		else return false ;
	}

	public WebElement selectDropDownByIndex(WebElement ele, int value) {
		
		try {
			Select a=new Select(ele);
			a.selectByIndex(value);
			
		} catch (Exception e) {
			System.out.println(" Index Selection failed");		
			e.printStackTrace();
		}
		return null;
	}

public WebElement selectDropDownByTextorValue(WebElement ele, String value) {
		
		try {
			Select c=new Select(ele);
			c.selectByVisibleText(value);
			
			
		} catch (Exception e) {
			System.out.println("  Selection failed");		
			e.printStackTrace();
		}
		return null;
	
}
	
	/****************************************************************************/

	
}