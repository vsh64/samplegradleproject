package TL.week6.Day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import cucumber.runtime.FeatureCompiler;

public class Parameters  extends ProjectSpecificMethod1{
	
	
	
	@Test(dataProvider="fetchdata")
	public void Login2(String comp,String fname , String lname ,String salutation, String flocname , String llocname ) {
		
		WebElement d = locate("LinkText", "CRM/SFA");
		click(d);
		WebElement e = locate("LinkText", "Create Lead");
		click(e);
		WebElement f = locate("id", "createLeadForm_companyName");
		enter(f, comp);
		WebElement g = locate("id", "createLeadForm_firstName");
		enter(g, fname);
		WebElement h = locate("id", "createLeadForm_lastName");
		enter(h, lname);
		WebElement i = locate("id", "createLeadForm_personalTitle");
		enter(i, salutation);
		WebElement j = locate("id", "createLeadForm_firstNameLocal");
		enter(j, flocname);
		WebElement k = locate("id", "createLeadForm_lastNameLocal");
		enter(k, llocname);
		WebElement l = locate("id", "createLeadForm_dataSourceId");
		selectDropDownByVisibleText(l, "Direct Mail");
		WebElement m = locate("id", "createLeadForm_industryEnumId");
		selectDropDownByVisibleText(m, "Finance");
		WebElement n = locate("class", "smallSubmit");
		click(n);
		driver.quit();
		System.out.println("CreateLead2");
	}
	
	

}
