package TL.week6.Day2;

import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelLearn {

	public static void main(String[] args) throws IOException {

		XSSFWorkbook wbook=new XSSFWorkbook("./Data/TestData.xlsx");
		XSSFSheet sheet = wbook.getSheet("Sheet1");
		int rowCount=sheet.getLastRowNum();
		int ColumnCount = sheet.getRow(0).getLastCellNum();

		for(int i=1 ;i <=rowCount;i++) {
		XSSFRow row = sheet.getRow(i);
		for (int j = 0; j <ColumnCount; j++) {
			XSSFCell cell = row.getCell(j);
		    String CellValue = cell.getStringCellValue();
			System.out.println("value is "+CellValue);
			System.out.println(cell);
		}
		}
	}

}
