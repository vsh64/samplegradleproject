package TL.week6.Day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TestCase1 extends ProjectSpecificMethod {
	
	@Test(enabled=false,invocationCount=2,timeOut=90000)
	public void Login1() {
		
		doLogin();
		WebElement d = locate("LinkText", "CRM/SFA");
		click(d);
		WebElement e = locate("LinkText", "Create Lead");
		click(e);
		WebElement f = locate("id", "createLeadForm_companyName");
		enter(f, "IBM");
		WebElement g = locate("id", "createLeadForm_firstName");
		enter(g, "Vishnu");
		WebElement h = locate("id", "createLeadForm_lastName");
		enter(h, "Vsh");
		WebElement i = locate("id", "createLeadForm_personalTitle");
		enter(i, "Mr.");
		WebElement j = locate("id", "createLeadForm_firstNameLocal");
		enter(j, "Vshu");
		WebElement k = locate("id", "createLeadForm_lastNameLocal");
		enter(k, "Vshuuuu");
		WebElement l = locate("id", "createLeadForm_dataSourceId");
		selectDropDownByVisibleText(l, "Direct Mail");
		WebElement m = locate("id", "createLeadForm_industryEnumId");
		selectDropDownByVisibleText(m, "Finance");
		WebElement n = locate("class", "smallSubmit");
		click(n);
		driver.quit();
		System.out.println("CreateLead1");

	}
		
		@Test(invocationCount=1,invocationTimeOut=100000)
		public void Login2() {
			
			doLogin();
			WebElement d = locate("LinkText", "CRM/SFA");
			click(d);
			WebElement e = locate("LinkText", "Create Lead");
			click(e);
			WebElement f = locate("id", "createLeadForm_companyName");
			enter(f, "IBM");
			WebElement g = locate("id", "createLeadForm_firstName");
			enter(g, "Vishnu");
			WebElement h = locate("id", "createLeadForm_lastName");
			enter(h, "Vsh");
			WebElement i = locate("id", "createLeadForm_personalTitle");
			enter(i, "Mr.");
			WebElement j = locate("id", "createLeadForm_firstNameLocal");
			enter(j, "Vshu");
			WebElement k = locate("id", "createLeadForm_lastNameLocal");
			enter(k, "Vshuuuu");
			WebElement l = locate("id", "createLeadForm_dataSourceId");
			selectDropDownByVisibleText(l, "Direct Mail");
			WebElement m = locate("id", "createLeadForm_industryEnumId");
			selectDropDownByVisibleText(m, "Finance");
			WebElement n = locate("class", "smallSubmit");
			click(n);
			driver.quit();
			System.out.println("CreateLead2");
		}
			
			@Test(dependsOnMethods = "Login4")
			public void Login3() {
				
				doLogin();
				WebElement d = locate("LinkText", "CRM/SFA");
				click(d);
				WebElement e = locate("LinkText", "Create Lead");
				click(e);
				WebElement f = locate("id", "createLeadForm_companyName");
				enter(f, "IBM");
				WebElement g = locate("id", "createLeadForm_firstName");
				enter(g, "Vishnu");
				WebElement h = locate("id", "createLeadForm_lastName");
				enter(h, "Vsh");
				WebElement i = locate("id", "createLeadForm_personalTitle");
				enter(i, "Mr.");
				WebElement j = locate("id", "createLeadForm_firstNameLocal");
				enter(j, "Vshu");
				WebElement k = locate("id", "createLeadForm_lastNameLocal");
				enter(k, "Vshuuuu");
				WebElement l = locate("id", "createLeadForm_dataSourceId");
				selectDropDownByVisibleText(l, "Direct Mail");
				WebElement m = locate("id", "createLeadForm_industryEnumId");
				selectDropDownByVisibleText(m, "Finance");
				WebElement n = locate("class", "smallSubmit");
				click(n);
				driver.quit();
				System.out.println("CreateLead3");

				
		
	}
			@Test(dependsOnMethods = "Login2")
			public void Login4() {
				
				doLogin();
				WebElement d = locate("LinkText", "CRM/SFA");
				click(d);
				WebElement e = locate("LinkText", "Create Lead");
				click(e);
				WebElement f = locate("id", "createLeadForm_companyName");
				enter(f, "IBM");
				WebElement g = locate("id", "createLeadForm_firstName");
				enter(g, "Vishnu");
				WebElement h = locate("id", "createLeadForm_lastName");
				enter(h, "Vsh");
				WebElement i = locate("id", "createLeadForm_personalTitle");
				enter(i, "Mr.");
				WebElement j = locate("id", "createLeadForm_firstNameLocal");
				enter(j, "Vshu");
				WebElement k = locate("id", "createLeadForm_lastNameLocal");
				enter(k, "Vshuuuu");
				WebElement l = locate("id", "createLeadForm_dataSourceId");
				selectDropDownByVisibleText(l, "Direct Mail");
				WebElement m = locate("id", "createLeadForm_industryEnumId");
				selectDropDownByVisibleText(m, "Finance");
				WebElement n = locate("class", "smallSubmit");
				click(n);
				driver.quit();
				System.out.println("CreateLead4");
}
@Test(alwaysRun=true)	
public void Login5() {
	System.out.println("always run");
}

@Test(enabled = false )
public void Login6() {
	System.out.println("enabled");
}
@Test(priority=-1)
public void Login7() {
	System.out.println("My first Test Priority");
}
@Test(priority=0)
public void Login8(){
	System.out.println("default priority");
}
@Test(dependsOnMethods="TL.week6.Day1.TestCase3.Login")
public void Login9() {
	System.out.println("In test Case 3 Login method");
}

@Test(expectedExceptions = RuntimeException.class)
public void Login10() {
	System.out.println("In Run Time exception . class");// report will be passed
}
}

