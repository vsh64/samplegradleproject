package TL.week6.Day1;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class MyMethods implements WedDriverMethods{
	
	  public RemoteWebDriver driver;
	  public int i=1;
	  
	@Override
	public void startApp(String browser, String url) {

	
		
		if(browser.equalsIgnoreCase("chrome")) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		}
		else if(browser.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", "./drivers/iexplorer.exe");
			 driver=new InternetExplorerDriver();}
		else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			 driver=new FirefoxDriver();
		}
		else System.out.println("Browser Not found");
			
		System.out.println(browser+ "Launched Successfully");
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		screenshot();
		
	}

	@Override
	public String enter(WebElement ele, String data) {
		
		ele.sendKeys(data);
		System.out.println(data+" entered successfully");
		
		return null;
	}

	@Override
	public void click(WebElement ele) {

		ele.click();
		System.out.println("Element Clicked");
		screenshot();
		
	}

	@Override
	public WebElement locate(String attribute, String value) {
		
		switch(attribute) {
		case "id": return driver.findElementById(value);
		case "name": return driver.findElementByName(value);
		case "xpath": return driver.findElementByXPath(value);
		case "LinkText": return driver.findElementByLinkText(value);
		case "TagName": return driver.findElementByTagName(value);
		case "cssSelector": return driver.findElementByCssSelector(value);
		case "class": return driver.findElementByClassName(value);
		default : System.out.println("Web element Located");
		}
		

		return null;
	}

	@Override
	public void screenshot() {
        
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest=new File("./snapshots/img["+i+"].png");
		try {
			FileUtils.copyFile(src, dest);
			System.out.println("Screenshot has been Taken Successfully");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public WebElement locate(String value) {
		
		driver.findElementById(value);
		
		return null;
	}

	@Override
	public WebElement selectDropDownByVisibleText(WebElement ele, String value) {
		
		Select dropdown=new Select(ele);
		dropdown.selectByVisibleText(value);
		screenshot();
		return null;
	}
	@Override
	public WebElement selectDropDownByIndex(WebElement ele, int value) {
		
		Select dropdown=new Select(ele);
		dropdown.selectByIndex(value);
		
		return null;
	}
	@Override
	public WebElement selectDropDownByValue(WebElement ele, String value) {
		
		Select dropdown=new Select(ele);
		dropdown.selectByValue(value);
		
		return null;
	}
	
	

}
