package TL.week6.Day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.beust.jcommander.Parameter;

public class ProjectSpecificMethod1 extends MyMethods{
	
	@Parameters({ "browser","url" , "uname", "pass"})
	@BeforeMethod
	public void doLogin(String browser , String url, String uname,String pass){

		
	startApp(browser,url);
	WebElement a = locate("name","USERNAME");
	enter(a, uname);
	WebElement b = locate("name", "PASSWORD");
	enter(b, pass);
	WebElement c = locate("class", "decorativeSubmit");
	click(c); 

	}

}
