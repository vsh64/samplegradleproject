package TL.week5.day1;

import org.openqa.selenium.WebElement;

public interface WedDriverMethods {
	
	public void startApp(String browser,String url);
	public String enter(WebElement ele , String data);
	public void click(WebElement ele );
	public WebElement locate(String attribute,String value);
	public void screenshot();
	public WebElement locate(String value);
	WebElement selectDropDownByVisibleText(WebElement ele, String value);
	WebElement selectDropDownByIndex(WebElement ele, int value);
	WebElement selectDropDownByValue(WebElement ele, String value);
	 

}
