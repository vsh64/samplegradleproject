package TL.week1.day1;

import java.util.Scanner;

public class DynamicEmpIDArrayInt {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Input");
		int size=sc.nextInt();
		int[] empID=new int[size];
		for(int i=0;i<size;i++)
			empID[i]=sc.nextInt();
		for(int i=0;i<size;i++)
			System.out.println(empID[i]);
		sc.close();
	}

}
