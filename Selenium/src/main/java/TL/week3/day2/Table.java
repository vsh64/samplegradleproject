package TL.week3.day2;

import java.awt.RenderingHints.Key;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Table {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver_64bit.exe");
		//FirefoxDriver driver= new FirefoxDriver();
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.ENTER);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("WL",Keys.ENTER);
		
		WebElement check = driver.findElementById("chkSelectDateOnly");
		if(check.isSelected())
			check.click();
		
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		
		List<WebElement> row = table.findElements(By.tagName("tr"));
		WebElement secondrowdata = row.get(1);
		String rowdata =secondrowdata.getText();
		System.out.println(rowdata);
		
		List<WebElement> column = table.findElements(By.tagName("td"));
		WebElement secondcolumn = column.get(2);
		String columndata = secondcolumn.getText();
		System.out.println(columndata);
		
			

		
		

		
		
		

	}

}
