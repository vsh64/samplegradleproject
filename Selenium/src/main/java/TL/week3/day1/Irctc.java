package TL.week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.findElementById("userRegistrationForm:userName").sendKeys("Vshu64");
		driver.findElementById("userRegistrationForm:password").sendKeys("Vishnu64");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Vishnu64");
		
		WebElement s1= driver.findElementById("userRegistrationForm:securityQ");
		Select a=new Select(s1);
		a.selectByVisibleText("What is your pet name?");
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Vshu");
		
		WebElement s2= driver.findElementById("userRegistrationForm:prelan");
		Select b=new Select(s2);
		b.selectByValue("en");
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Vishnu");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("K");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("V");
		
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		
		WebElement s3= driver.findElementById("userRegistrationForm:dobDay");
		Select c=new Select(s3);
		c.selectByValue("26");
		
		
		WebElement s4= driver.findElementById("userRegistrationForm:dobMonth");
		Select d=new Select(s4);
		d.selectByValue("03");
		
		WebElement s5= driver.findElementById("userRegistrationForm:dateOfBirth");
		Select e=new Select(s5);
		e.selectByValue("1994");

		WebElement s6= driver.findElementById("userRegistrationForm:occupation");
		Select f=new Select(s6);
		f.selectByValue("4");

		
		driver.findElementById("userRegistrationForm:uidno").sendKeys("316112422214");
		driver.findElementById("userRegistrationForm:idno").sendKeys("CTBPK2081H");
		
		WebElement s7= driver.findElementById("userRegistrationForm:countries");
		Select g=new Select(s7);
		g.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:email").sendKeys("vishnu.vsh123@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("7396781782");

		WebElement s10= driver.findElementById("userRegistrationForm:nationalityId");
		Select j=new Select(s10);
		j.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("12-5-484");
		driver.findElementById("userRegistrationForm:street").sendKeys("Mugalivakkam");
		driver.findElementById("userRegistrationForm:area").sendKeys("Sabari Nagar");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600125",Keys.TAB);

		Thread.sleep(3000);
		
		WebElement s8= driver.findElementById("userRegistrationForm:cityName");
		Select h=new Select(s8);
		h.selectByValue("Kanchipuram");
		
		Thread.sleep(3000);
		
		WebElement s9= driver.findElementById("userRegistrationForm:postofficeName");
		Select i=new Select(s9);
		i.selectByVisibleText("Mugalivakkam S.O");
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("7396781782");
		driver.findElementById("userRegistrationForm:resAndOff:0").click();
		
		System.out.println("Account Created");
	}

}
