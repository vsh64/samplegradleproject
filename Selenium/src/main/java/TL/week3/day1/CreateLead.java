package TL.week3.day1;



import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver_64bit.exe");
		//FirefoxDriver driver= new FirefoxDriver();
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("IBM");
		driver.findElementById("createLeadForm_firstName").sendKeys("Vishnu");
		driver.findElementById("createLeadForm_lastName").sendKeys("Vsh");
		WebElement s1 = driver.findElementById("createLeadForm_dataSourceId");
		WebElement s2 = driver.findElementById("createLeadForm_marketingCampaignId");
		WebElement s3 = driver.findElementById("createLeadForm_industryEnumId");
		

		Select a = new Select(s1);
		a.selectByVisibleText("Public Relations");
		Select b=new Select(s2);
		b.selectByValue("CATRQ_CARNDRIVER");
		Select c=new Select(s3);

		List<WebElement> all=c.getOptions();
		int count = all.size();
		System.out.println(count);
		c.selectByIndex(count-3);
		
		driver.findElementByName("submitButton").click();
		System.out.println("Lead Created");


	//	driver.close();


		
		
	}

}
