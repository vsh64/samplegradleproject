package TL.week2.day2;

import java.util.Map;
import java.util.TreeMap;

public class MapSample {

	public static void main(String[] args) {

	String str= "Paypal India";
	
	Map<Character,Integer> map1=new TreeMap<Character,Integer>();
	
	char[] chArray = str.toLowerCase().toCharArray();
	for (char eachCharacter : chArray) {
		if(map1.containsKey(eachCharacter))
		{
			map1.put(eachCharacter, map1.get(eachCharacter)+1);
		}
		else 
		{
			map1.put(eachCharacter, 1);
		}
	}
	System.out.println(map1);
	}
	}


