package TL.week2.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ListAndSet {

	public static void main(String[] args) {
		
		Set<String> set= new TreeSet<String>();
		set.add("Vignesh");
		set.add("Nattu");
		set.add("Kannan");
		set.add("Vignesh");
		System.out.println(set);
	//	for (String Name : set) {System.out.println(Name);}
		
	   List<String> list=new ArrayList<String>();
		
	   list.addAll(set);
	   String LastButOne = list.get(list.size()-2);
	   System.out.println("Last But one is :"+LastButOne);
	   list.add("Kannan");
	   System.out.println("added kanan:"+list);
	   list.remove("Vignesh");
	   System.out.println("After Removing vignesh "+list);
	   
	   System.out.println(list.size());
		
		

	}

}
