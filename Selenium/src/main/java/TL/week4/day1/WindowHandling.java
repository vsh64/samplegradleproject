package TL.week4.day1;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandling {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("IBM");
		driver.findElementById("createLeadForm_firstName").sendKeys("Vishnu");
		driver.findElementById("createLeadForm_lastName").sendKeys("Vsh");
		driver.findElementByXPath("//img[@alt='Lookup']").click();
		
		
		
		Set<String> allWindows = driver.getWindowHandles();
		List<String> lst=new ArrayList<>();
		lst.addAll(allWindows);
		driver.switchTo().window(lst.get(0));
		driver.findElementByLinkText("//a[@class='linktext']").click();
		System.out.println("Clicked");
		driver.switchTo().defaultContent();
		
	/*	
		WebElement table = driver.findElementByXPath("(//table)[4]");
		List<WebElement> row = table.findElements(By.tagName("tr"));
		WebElement secondrowdata = row.get(0);
		String rowdata =secondrowdata.getText();
		System.out.println(rowdata);
		
		List<WebElement> column = table.findElements(By.tagName("td"));
		WebElement secondcolumn = column.get(0);
		String columndata = secondcolumn.getText();
		System.out.println(columndata);*/
		
		//driver.close();
		
		
		
		
		
		
	}

}
