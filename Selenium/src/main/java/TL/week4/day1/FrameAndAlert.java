	package TL.week4.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class FrameAndAlert {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();
		
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		System.out.println("clicked");
		driver.switchTo().alert().sendKeys("Vishnu");
		driver.switchTo().alert().accept();
		System.out.println("alert ");
		String a =driver.findElementByXPath("//p[text()='Hello Vishnu! How are you today?']").getText();
	    System.out.println(a);
	    String b = "Hello Vishnu! How are you today?";
	    if(a.equals(b))
	    	System.out.println("Validation done");
	    driver.close();
		
		
		

		
	}

}

